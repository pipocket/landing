<?php

class Model_Register extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'email',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);
	protected static $_table_name = 'register';

	public function attributes( $post )
	{
		foreach ($post as $attr => $value)
		{
			if ($value)
			{
				$this->$attr = $value;
			}
		}
	}

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('email', 'email', 'required|valid_email');

		return $val;
	}
}
