<?php

class Controller_Home extends Controller_Template
{

	public function action_index()
	{
		if (Input::post('email'))
		{
			$validate = Model_Register::validate('register');
			$email = Input::post('email');
			$count = Model_Register::query()->where('email', $email)->count();
			$status = true;

			if ($validate->run())
			{
				if (!$count)
				{
					$model = new Model_Register;
					$model->email = Input::post('email');

					if (!$model->save())
					{
						$status = false;
					}
				}
			}
			else
			{
				$status = false;
			}

			Session::set_flash('register', $status);
		}

		$this->template->content = View::forge('home/index');
	}

	public function action_404()
	{
		$this->template->content = View::forge('home/404');
	}

}
