<div class="top">
	<div class="container">
		<div class="row">
			<h1><a href="<?php echo Uri::base(false) ?>"><img src="assets/img/logo.png" alt="Pipocket brand" title="Pipocket"></a></h1>
		</div>
		<div class="row">
			<p>As coisas estão mudando por aqui, e muito em breve voltaremos com novidades.</p>
		</div>
	</div>
</div>

<div class="bottom">
	<div class="container">
		<div class="row">
			<?php if (Session::get_flash('register')===null): ?>
			<form action="" method="POST" class="form-group">
				<fieldset>
					<input type="text" name="email" class="form-control input-lg" placeholder="Informe seu e-mail">
					<input type="submit" class="btn btn-success btn-lg" value="Me avise">
				</fieldset>
			</form>
			<?php else: ?>
				<?php if (Session::get_flash('register')): ?>
				<p class="success">Obrigado, lhe avisaremos assim que tudo estiver pronto :)</p>
				<?php else: ?>
				<p class="error">Ops, ocorreu um erro ao tentar registrar seu e-mail, ente novamente mais tarde :(</p>
				<?php endif ?>
			<?php endif ?>
		</div>
	</div>
</div>